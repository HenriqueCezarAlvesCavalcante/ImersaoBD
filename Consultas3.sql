CREATE TABLE Hospedes (
	CPF VARCHAR(12) NOT NULL,
	nome VARCHAR(50) NOT NULL,
	endereco VARCHAR(50),
	sexo CHAR(1),
	dataNascimento TIMESTAMP,

	PRIMARY KEY(CPF),   
	CHECK (sexo in ('M', 'F'))
);

INSERT INTO Hospedes VALUES ('15987425691', 'Maria dos Santos', 'Av. Epitácio Pessoa, 21, João Pessoa', 'F', '1960-02-06');
INSERT INTO Hospedes VALUES ('54722165892', 'João Augusto Trindade', 'R. das Flores, 33, Recife', 'M', '1980-05-12');
INSERT INTO Hospedes VALUES ('52265548892', 'Ana Maria Pereira', 'R. Augusta, 51, São Paulo', 'F', '1972-08-09');
INSERT INTO Hospedes VALUES ('92241103584', 'Luiza Costa', 'Av. Epitácio Pessoa, 297, João Pessoa', 'F', '1985-12-11');
INSERT INTO Hospedes VALUES ('24169951200', 'Francisco Chaves', 'R. Aurora, 95, Maceió', 'M', '1955-09-06');
INSERT INTO Hospedes VALUES ('62100038522', 'Antônio Alves', 'R. José Firmino, 33, Rio de Janeiro', 'M', '1980-05-09');


CREATE TABLE Quartos (
	numero INT NOT NULL,
	tipo VARCHAR(40),
	valorDiaria NUMERIC(6,2),

	PRIMARY KEY (numero),
	CHECK(valorDiaria > 0)
);

INSERT INTO Quartos VALUES (220, 'Standard', 150.60);
INSERT INTO Quartos VALUES (230, 'Suíte Master', 500);
INSERT INTO Quartos VALUES (240, 'Luxo Superior', 320);
INSERT INTO Quartos VALUES (250, 'Suíte Presidencial', 850);
INSERT INTO Quartos VALUES (320, 'Standard', 150.60);
INSERT INTO Quartos VALUES (330, 'Luxo Superior', 320);
INSERT INTO Quartos VALUES (340, 'Suíte Master', 500);
CREATE TABLE Estadias (
	CPF CHAR(11) NOT NULL,
	numero INT NOT NULL,
	dataEntrada TIMESTAMP,
	dataSaida TIMESTAMP,

	PRIMARY KEY(CPF, dataEntrada),
	FOREIGN KEY (CPF) REFERENCES Hospedes(CPF),
	FOREIGN KEY (numero) REFERENCES Quartos,
	CHECK (dataSaida > dataEntrada)
);

INSERT INTO Estadias VALUES ('15987425691', 220, '2007-01-01', '2007-01-03');
INSERT INTO Estadias VALUES ('54722165892', 230, '2008-01-01', '2008-01-10');
INSERT INTO Estadias VALUES ('52265548892', 340, '2007-08-09', '2007-08-12');
INSERT INTO Estadias VALUES ('15987425691', 330, '2008-02-05', '2008-02-09');
INSERT INTO Estadias VALUES ('92241103584', 250, '2008-07-05', '2008-07-10');
INSERT INTO Estadias VALUES ('24169951200', 220, '2008-05-01', '2008-05-05');
INSERT INTO Estadias VALUES ('62100038522', 330, '2008-03-03', '2008-03-09');
INSERT INTO Estadias VALUES ('62100038522', 340, '2008-09-01', '2008-09-04');
INSERT INTO Estadias VALUES ('54722165892', 330, '2008-06-01', '2008-06-10');
INSERT INTO Estadias VALUES ('92241103584', 250, '2008-10-10', '2008-10-12');

CREATE TABLE Servicos (
	codServico INT NOT NULL,
	descricao VARCHAR(40),
	preco NUMERIC(6,2),

	PRIMARY KEY(codServico),
	CHECK(preco > 0)
);
INSERT INTO Servicos VALUES (1, 'Lavanderia', 15);
INSERT INTO Servicos VALUES (2, 'Passadeira', 25.50);
INSERT INTO Servicos VALUES (3, 'Babá', 35.90);
INSERT INTO Servicos VALUES (4, 'Café no Quarto', 24.50);
INSERT INTO Servicos VALUES (5, 'Lanchonete', 33);

CREATE TABLE Solicitacoes (
	CPF CHAR(11) NOT NULL,
	codServico INT NOT NULL,
	dataPedido TIMESTAMP,
	
	PRIMARY KEY(CPF, dataPedido),
	FOREIGN KEY (CPF) REFERENCES Hospedes(CPF),
	FOREIGN KEY (codServico) REFERENCES Servicos(codServico)
);
INSERT INTO Solicitacoes VALUES ('15987425691', 1, '2007-01-01');
INSERT INTO Solicitacoes VALUES ('54722165892', 5, '2008-01-01');
INSERT INTO Solicitacoes VALUES ('62100038522', 3, '2008-09-03');
INSERT INTO Solicitacoes VALUES ('92241103584', 1, '2008-10-12');
INSERT INTO Solicitacoes VALUES ('92241103584', 4, '2008-07-10');

