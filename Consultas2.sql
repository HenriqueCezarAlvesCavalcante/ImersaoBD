-- SELECT * FROM Pais
INSERT INTO Pais VALUES ('BRA','Brasil');
INSERT INTO Pais VALUES ('ARG','Argentina');
INSERT INTO Pais VALUES ('ALE','Alemanha');
INSERT INTO Pais VALUES ('CHI','Chile');

-- SELECT * FROM Cidade
INSERT INTO Cidade VALUES (1, 'João Pessoa', 'PB', 'BRA');
INSERT INTO Cidade VALUES (2, 'Campina Grande', 'PB', 'BRA');
INSERT INTO Cidade VALUES (3, 'Recife', 'PE', 'BRA');
INSERT INTO Cidade VALUES (4, 'Buenos Aires', 'BA', 'ARG');
INSERT INTO Cidade VALUES (5, 'Santiago', 'ST', 'CHI');
INSERT INTO Cidade VALUES (6, 'Hamburgo', 'HB', 'ALE');
INSERT INTO Cidade VALUES (7, 'Rio de Janeiro', 'RJ', 'BRA');

-- SELECT * FROM Cliente
INSERT INTO Cliente (codigo, nome, tipo, contato, cargo, endereco, cidade, cep, fone) VALUES (1, 'Maria Helena Ferreira', 'PF', 'Helena', 'Medica', 'R. das Flores, 33, Manaira', '1', '58.045-002', '3225-6987');
INSERT INTO Cliente (codigo, nome, tipo, contato, cargo, endereco, cidade, cep, fone) VALUES (2, 'João Paulo Correia', 'PF', 'Paulo', 'Advogado', 'R. das Acacias, 920, Bessa', '1', '58.037-255', '3245-8999');
INSERT INTO Cliente (codigo, nome, tipo, contato, cargo, endereco, cidade, cep, fone) VALUES (3, 'Larissa Maia', 'PF', 'Larissa', 'Estudante', 'R. das Flores, 95, Manaira', '1', '58.045-002', '3227-9622');
INSERT INTO Cliente (codigo, nome, tipo, contato, cargo, endereco, cidade, cep, fone) VALUES (4, 'Marcos Andrade', 'PF', 'Marcos', 'Analista de Sistemas', 'Av. Juruá, 95', '2', '58.000-00', '3338-2100');
INSERT INTO Cliente (codigo, nome, tipo, endereco, cidade, cep, fone) VALUES (5, 'LT Comércio', 'PJ', 'Av. Ingá, 97', '3', '56.000-00', '3256-9981');
INSERT INTO Cliente (codigo, nome, tipo, endereco, cidade, cep, fone) VALUES (6, 'JR Representações', 'PJ', 'Av. Maracatu, 665', '7', '21.654-00', '5841-9871');

-- SELECT * FROM Tipo
INSERT INTO Tipo (codigo, nome) VALUES (1, 'Lacticínios');
INSERT INTO Tipo (codigo, nome) VALUES (2, 'Tecidos');
INSERT INTO Tipo (codigo, nome) VALUES (3, 'Alimentação');
INSERT INTO Tipo (codigo, nome) VALUES (4, 'Cama');
INSERT INTO Tipo (codigo, nome) VALUES (5, 'Decoração');
INSERT INTO Tipo (codigo, nome) VALUES (6, 'Eletro');

-- SELECT * FROM Produto
INSERT INTO Produto VALUES (1, 'Toalhas Artex', 'Conjunto de Toalhas', 'Toalha Kings', 210, 125, 50, 25, 4, 'N');
INSERT INTO Produto VALUES (2, 'Iogurte Nestle', 'Conjunto com 6 potes de Iogurte', 'Danoninho', 3.20, 1.35, 200, 150, 1, 'N');
INSERT INTO Produto VALUES (3, 'Abajur Alist', 'Abajur de vidro e cristal', 'Abajur', 364, 290, 12, 6, 5, 'S');
INSERT INTO Produto VALUES (4, 'TV Plasma', 'TV de Plasma 42 polegadas', 'TV Plasma', 3.654, 2.650, 12, 9, 6, 'N');
INSERT INTO Produto VALUES (5, 'Liquidificador', 'Liquidificador Arno 6 posições', 'Liquidificador Dmais', 92, 58, 150, 100, 6, 'S');
INSERT INTO Produto VALUES (6, 'Computador Positivo', 'Computador Pentium IV', 'Positivo PIV', 1.980, 1.600, 24, 13, 6, 'D');
INSERT INTO Produto VALUES (7, 'Pizza Sadia', 'Pizza tamanho médio Sadia', 'Pizza Sadia', 6.90, 4.10, 541, 200, 3, 'N');

-- SELECT * FROM Funcao
INSERT INTO Funcao VALUES (1, 'Vendedor', 200 );
INSERT INTO Funcao VALUES (2, 'Auxiliar de Caixa', 126);
INSERT INTO Funcao VALUES (3, 'Gerente', 500);
INSERT INTO Funcao VALUES (4, 'Segurança', 300);

-- SELECT * FROM Setor
INSERT INTO Setor (Sigla, nome) VALUES ('COV', 'Compra e Venda');
INSERT INTO Setor (Sigla, nome) VALUES ('MKT', 'Marketing');
INSERT INTO Setor (Sigla, nome) VALUES ('SEG', 'Segurança');
INSERT INTO Setor (Sigla, nome) VALUES ('ADM', 'Administração');

-- SELECT * FROM Funcionario
INSERT INTO Funcionario (codigo, nome, sexo, estcivil, rg, cpf, datanasc, naturalidade, dataadm, endereco, bairro, cidade, funcao, setor, salario ) VALUES (1, 'João da Silva', 'M', 'S', '2541399', '04598722354', '1978-02-05', 1, '2005-06-09', 'R. Uire, 98', 'Tambaú', 1, 1, 'COV', 250);
INSERT INTO Funcionario (codigo, nome, sexo, estcivil, rg, cpf, datanasc, naturalidade, dataadm, endereco, bairro, cidade, funcao, setor, salario ) VALUES (2, 'Maria de Souza', 'F', 'C', '0145687', '24598711200', '1970-01-01', 3, '2000-09-08', 'R. Umbuzeiro, 12', 'Manaíra', 1, 2, 'MKT', 300);
INSERT INTO Funcionario (codigo, nome, sexo, estcivil, rg, cpf, datanasc, naturalidade, dataadm, endereco, bairro, cidade, funcao, setor, salario ) VALUES (3, 'Luiza Costa', 'F', 'C', '2185411', '36574100296', '1980-04-04', 7, '1998-05-01', 'A. Esperança, 91', 'Bessa', 1, 3, 'ADM', 950);
INSERT INTO Funcionario (codigo, nome, sexo, estcivil, rg, cpf, datanasc, naturalidade, dataadm, endereco, bairro, cidade, funcao, setor, salario, email ) VALUES (4, 'Francisco da Silva', 'M', 'D', '0584132', '01487933587', '1970-06-03', 6, '2002-08-05', 'Av. Fagundes, 05', 'Mangabeira', 1, 4, 'SEG', 390, 'franc@uol.com.br');
INSERT INTO Funcionario (codigo, nome, sexo, estcivil, rg, cpf, datanasc, naturalidade, dataadm, endereco, bairro, cidade, funcao, setor, salario, email ) VALUES (5, 'Carla Tavares', 'F', 'C', '2987411', '36544800298', '1969-01-08', 5, '1992-03-01', 'R. Uire, 154', 'Tambaú', 1, 2, 'COV', 290, 'carla@gmail.com');

-- SELECT * FROM Pedido
INSERT INTO Pedido VALUES (1, 2, 1, '2007-05-09', '2005-06-01', 'T',1200);
INSERT INTO Pedido VALUES (2, 5, 1, '2007-01-12', '2005-02-12', 'A',100);
INSERT INTO Pedido VALUES (3, 6, 2, '2006-10-02', '2006-11-11', 'A',254);
INSERT INTO Pedido VALUES (4, 3, 2, '2007-01-01', '2006-01-01', 'A',569);
INSERT INTO Pedido VALUES (5, 1, 2, '2006-12-05', '2006-12-05', 'M',0);
INSERT INTO Pedido VALUES (6, 4, 3, '2006-03-09', '2006-03-09', 'M',0);
INSERT INTO Pedido VALUES (7, 1, 2, '2006-08-08', '2006-09-09', 'T',169);
INSERT INTO Pedido VALUES (8, 6, 4, '2007-03-02', '2007-03-09', 'M',0);
INSERT INTO Pedido VALUES (9, 1, 3, '2006-07-07', '2007-07-08', 'A',950);
INSERT INTO Pedido VALUES (10, 1, 2, '2007-06-02', '2007-06-02', 'M',0);
INSERT INTO Pedido VALUES (11, 6, 3, '2007-12-02', '2007-12-03', 'T',0);
INSERT INTO Pedido VALUES (12, 1, 2, '2007-10-10', '2007-11-01', 'M',658);
INSERT INTO Pedido VALUES (13, 4, 1, '2007-11-11', '2007-11-11', 'M',0);
INSERT INTO Pedido VALUES (14, 4, 2, '2008-01-12', '2008-01-01', 'T',126);
INSERT INTO Pedido VALUES (15, 6, 2, '2008-01-10', '2008-02-01', 'A',1500);
INSERT INTO Pedido VALUES (16, 1, 4, '2008-08-05', '2008-08-05', 'T',0);
INSERT INTO Pedido VALUES (17, 6, 4, '2008-07-10', '2008-07-10', 'T',0);
INSERT INTO Pedido VALUES (18, 2, 3, '2008-07-01', '2008-07-01', 'T',0);
INSERT INTO Pedido VALUES (19, 6, 2, '2008-06-12', '2008-06-12', 'T',0);
INSERT INTO Pedido VALUES (20, 5, 1, '2008-08-01', '2008-08-02', 'T',0);
INSERT INTO Pedido VALUES (21, 3, 3, '2008-11-05', '2009-11-05', 'M',147);
INSERT INTO Pedido VALUES (22, 3, 2, '2008-12-10', '2009-12-10', 'A',1900);
INSERT INTO Pedido VALUES (23, 1, 2, '2009-01-03', '2010-01-03', 'T',0);
INSERT INTO Pedido VALUES (24, 6, 2, '2009-01-01', '2010-10-01', 'T',0);
INSERT INTO Pedido VALUES (25, 4, 1, '2009-02-02', '2010-10-10', 'A',120);

-- SELECT * FROM Itens
INSERT INTO Itens VALUES (1, 2, 32, 10, 0.2);
INSERT INTO Itens VALUES (1, 3, 364, 1, 0.15);
INSERT INTO Itens VALUES (2, 1, 420, 2, 0.1);
INSERT INTO Itens VALUES (3, 5, 92, 1, 0.1);
INSERT INTO Itens VALUES (4, 7, 20.7, 3, 12.6);
INSERT INTO Itens VALUES (5, 1, 240, 1, 0.1);
INSERT INTO Itens VALUES (6, 1, 660, 3, 0.1);
INSERT INTO Itens VALUES (6, 4, 3.654, 1, 0.2);
INSERT INTO Itens VALUES (7, 4, 7.308, 2, 0.2);
INSERT INTO Itens VALUES (8, 7, 6.90, 1, 1.3);
INSERT INTO Itens VALUES (8, 2, 3.20, 1, 1.3);
INSERT INTO Itens VALUES (9, 1, 630, 3, 1.4);
INSERT INTO Itens VALUES (10, 1, 210, 1, 0.2);
INSERT INTO Itens VALUES (11,5, 92, 1, 0.0);
INSERT INTO Itens VALUES (12, 1, 210, 1, 0.2);
INSERT INTO Itens VALUES (13, 7, 13.80, 2, 0.0);
INSERT INTO Itens VALUES (14, 7, 6.90, 1, 0.0);
INSERT INTO Itens VALUES (15, 4, 3.654, 1, 0.2);
INSERT INTO Itens VALUES (16, 7, 3.9, 1, 0.2);
INSERT INTO Itens VALUES (17, 1, 14.95, 6, 0.0);
INSERT INTO Itens VALUES (18, 2, 2845, 2, 0);
INSERT INTO Itens VALUES (19, 7, 58.951, 3, 0);
INSERT INTO Itens VALUES (20, 4, 956, 5, 1.0);
INSERT INTO Itens VALUES (21, 4, 956, 5, 1.0);
INSERT INTO Itens VALUES (22, 7, 6.90, 1, 0);
INSERT INTO Itens VALUES (23, 1, 630, 3, 1.4);
INSERT INTO Itens VALUES (24, 7, 20.7, 3, 12.6);
INSERT INTO Itens VALUES (25, 2, 32, 10, 0.2);
INSERT INTO Itens VALUES (25, 1, 2845, 2, 0);